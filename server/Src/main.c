/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "string.h"

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;
unsigned int i=0;
//char transmitBuffer [ 52 ] ; 
char receiveBuffer [ 1000 ] ;
QueueHandle_t xQueue1;
unsigned int val= 0;
unsigned int Rval= 0;
uint8_t t=0;
 uint8_t o=0;
 uint8_t x=0;

 
char a[70];
int Light[4]={0,0,0,0};
unsigned int id=0;
unsigned int check=0;
unsigned int option=0;
unsigned int reQ=0;
unsigned int c2=0;
size_t y=1;
char b[1000];
 



/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
void StartDefaultTask(void const * argument);

void vTaskReceive(void *pvParameters)
{
	
	while(1)
		{
			
				HAL_UART_Receive_IT(&huart2, b, 1000);
	
				strcpy(receiveBuffer,b);
				if(strstr(receiveBuffer,"GET /01")) Light[0]=1;
				if(strstr(receiveBuffer,"GET /00")) Light[0]=0;
				if(strstr(receiveBuffer,"GET /02")) Light[0]=2;
				if(strstr(receiveBuffer,"GET /10")) Light[1]=0;
				if(strstr(receiveBuffer,"GET /11")) Light[1]=1;
				if(strstr(receiveBuffer,"GET /12")) Light[1]=2;
				if(strstr(receiveBuffer,"GET /20")) Light[2]=0;
				if(strstr(receiveBuffer,"GET /21")) Light[2]=1;
				if(strstr(receiveBuffer,"GET /22")) Light[2]=2;
				if(strstr(receiveBuffer,"GET /30")) Light[3]=0;
				if(strstr(receiveBuffer,"GET /31")) Light[3]=1;
				if(strstr(receiveBuffer,"GET /32")) Light[3]=2;
				if(strstr(receiveBuffer,"GET /check")) check=1;
				if(strstr(receiveBuffer,"OPTIONS")) option=1;
				if(strstr(receiveBuffer,"+IPD,0")) {id=0;reQ=1;}
				if(strstr(receiveBuffer,"+IPD,1")) {id=1;reQ=2;}
				if(strstr(receiveBuffer,"+IPD,2")) {id=2;reQ=3;}
				if(strstr(receiveBuffer,"+IPD,3")) {id=3;reQ=4;}
				if(strstr(receiveBuffer,"+IPD,4")) {id=4;reQ=5;}
				if(strstr(receiveBuffer,"CLOSE")) {reQ=0;}
				
				for(i=0;i<1000;i++)
					{
							b[i]=0;
					}
					vTaskDelay(2000);

				huart2.pRxBuffPtr=b;
		}
}
 void vTaskTransmit(void *pvParameters)
	 {
		 y=0;
					huart2.pTxBuffPtr=a;
				 	strcpy(a,"AT+CWMODE=1\r\n");
					HAL_UART_Transmit_IT(&huart2,a,50);
					vTaskDelay(7000);
					huart2.pTxBuffPtr=a;
				 	strcpy(a,"AT+CIPMUX=1\r\n");
					HAL_UART_Transmit_IT(&huart2,a,50);
					vTaskDelay(7000);
					huart2.pTxBuffPtr=a;
				 	strcpy(a,"AT+CWJAP=\"Tenno\",\"12345678\"\r\n");
					HAL_UART_Transmit_IT(&huart2,a,50);
					vTaskDelay(10000);				
					huart2.pTxBuffPtr=a;
				 	strcpy(a,"AT+CIPSERVER=1,80\r\n");
					HAL_UART_Transmit_IT(&huart2,a,50);
 
				while(1)
			 {
				 
								huart2.pTxBuffPtr=a;
				 if(option==0)
					 {
						 if(check==0)
									 {
												 if(reQ==1)
											 {
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPSEND=0,17\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"HTTP/1.1 200 OK\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPCLOSE=0\r\n");
															reQ=0;
															strcpy(b,"");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
											}
												 if(reQ==2)
											 {
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPSEND=1,17\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"HTTP/1.1 200 OK\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPCLOSE=1\r\n");
															reQ=0;
															strcpy(b,"");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
											}
												 if(reQ==3)
											 {
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPSEND=2,17\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"HTTP/1.1 200 OK\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPCLOSE=2\r\n");
															reQ=0;
															strcpy(b,"");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
											}
												 if(reQ==4)
											 {
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPSEND=3,17\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"HTTP/1.1 200 OK\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPCLOSE=3\r\n");
															reQ=0;
															strcpy(b,"");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
											}
												 if(reQ==5)
											 {
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPSEND=4,17\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"HTTP/1.1 200 OK\r\n");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
															huart2.pTxBuffPtr=a;
															strcpy(a,"AT+CIPCLOSE=4\r\n");
															reQ=0;
															strcpy(b,"");
															HAL_UART_Transmit_IT(&huart2,a,50);
															HAL_Delay(2000);
											}
										}
							else
							{
											if(reQ==1)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=0,55\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\nxxxx\r\n");
													HAL_UART_Transmit_IT(&huart2,a,70);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=0\r\n");
													reQ=0;
													check=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}
									 	 if(reQ==2)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=1,55\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\nxxxx\r\n");
													HAL_UART_Transmit_IT(&huart2,a,70);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=1\r\n");
													reQ=0;
													check=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}
									 	 if(reQ==3)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=2,55\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\nxxxx\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=2\r\n");
													reQ=0;
													check=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}
									 	 if(reQ==4)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=3,55\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\nxxxx\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=3\r\n");
													reQ=0;
													check=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}
									 	 if(reQ==5)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=4,55\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\nxxxx\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=4\r\n");
													reQ=0;
													check=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}	 
							}
					 }
					 else
						 {
												if(reQ==1)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=0,49\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=0\r\n");
													reQ=0;
													option=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}
									 	 if(reQ==2)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=1,49\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=1\r\n");
													reQ=0;
													option=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}
									 	 if(reQ==3)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=2,49\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=2\r\n");
													reQ=0;
													option=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}
									 	 if(reQ==4)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=3,49\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=3\r\n");
													reQ=0;
													option=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}
									 	 if(reQ==5)
									 {
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPSEND=4,49\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\n\r\n");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
													huart2.pTxBuffPtr=a;
													strcpy(a,"AT+CIPCLOSE=4\r\n");
													reQ=0;
													option=0;
													strcpy(b,"");
													HAL_UART_Transmit_IT(&huart2,a,50);
													HAL_Delay(2000);
									}	 
						 }
				vTaskDelay(20);
					y++;
			}
		}
 
 
void vTaskControl(void *pvParameters)
	 {
		 
		 while(1)
		 {
			 while (HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_7))
								 {
											 val=0;
											 xQueueSend(xQueue1,(void *)&val,( TickType_t ) 0 );								 
											 HAL_Delay(50);
											 t=1;
											 o=0;
									}
									if(t)
									{		
											val=0;
											xQueueSend(xQueue1,(void *)&val,( TickType_t ) 0 );
											 HAL_Delay(500);
											o++;
											if (o==8){ t= 0;   }
									}								
									else
									{									 
											val=1;
											xQueueSend(xQueue1,(void *)&val,( TickType_t ) 0 );	 
									}
			  
			if(xQueue1)
				{	
					 if( xQueueReceive(xQueue1,(void *)&Rval,( TickType_t ) 10 ))
					 { 
						 if(Light[0]==2) HAL_GPIO_WritePin(GPIOD,GPIO_PIN_1,Rval);
						 if(Light[1]==2) HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,Rval);
						 if(Light[2]==2) HAL_GPIO_WritePin(GPIOD,GPIO_PIN_3,Rval);
						 if(Light[3]==2) HAL_GPIO_WritePin(GPIOD,GPIO_PIN_4,Rval);
					 }		
				}
				
						 if(Light[0]!=2) {HAL_GPIO_WritePin(GPIOD,GPIO_PIN_1,Light[0]);}
						 if(Light[1]!=2) HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,Light[1]);
						 if(Light[2]!=2) HAL_GPIO_WritePin(GPIOD,GPIO_PIN_3,Light[2]);
						 if(Light[3]!=2) HAL_GPIO_WritePin(GPIOD,GPIO_PIN_4,Light[3]);
				vTaskDelay(1);
		 }
		 
	}

	
		int main(void)

{
		 

  /* MCU Configuration----------------------------------------------------------*/
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();
  /* Configure the system clock */
  SystemClock_Config();
  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();

	xQueue1 =xQueueCreate(15,sizeof(unsigned int ));
				
	
	
	xTaskCreate( vTaskReceive, "TaskReceive", 1000, NULL, 5, NULL);
	xTaskCreate( vTaskTransmit, "TaskTransmit", 1000, NULL, 4, NULL);
	xTaskCreate( vTaskControl, "TaskControl", 1000, NULL, 4, NULL);
	
  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

   
  while (1)
  {
   

  }
   

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4, GPIO_PIN_SET);

  /*Configure GPIO pins : PD1 PD2 PD3 PD4 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PD7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

 

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

   
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
