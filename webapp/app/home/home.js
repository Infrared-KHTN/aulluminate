'use strict';

var app = angular.module('myApp.home', ['ngRoute', 'angular-alert-banner', 'ui.bootstrap'])

// declared route
app.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/home', {
			templateUrl: 'home/home.html',
			controller: 'HomeCtrl'
		});
}])

// home controller
app.controller('HomeCtrl', function($scope, $http, AlertBanner) {
    $scope.radioModel1 = '21';
    $scope.radioModel2 = '22';
    $scope.radioModel3 = '23';
    $scope.radioModel4 = '24';
    $scope.sendSignal = function(param) {
		var hostname = 'http://192.168.43.115';
        // Override $http transform to not transform response
        // $http.defaults.transformResponse = undefined;
        // Hardcoding is bad, but I'm desperate
        $http({
            method: 'GET',
            url: hostname + '/' + param,
            timeout: 5000,
            transformResponse: null,
            transformRequest: null,
            withCredentials: true,
            headers: {
                'Accept': undefined,
                'Content-Type': 'text/plain'
            }
        }).then(function(response) {
            AlertBanner.publish({
                type: 'info',
                message: 'Received HTTP response: ' + response.data
            });
            $scope.radioModel1 = param;
        }, function(data) {
                AlertBanner.publish({
                    type: 'error',
                    message: 'HTTP request returned error: ' 
                      + data.status + ' ' + data.error
                });
            });
    }
});
