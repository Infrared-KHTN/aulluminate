'use strict;'

angular.module('myApp', [
	'ngRoute',
	'myApp.home'
]).
config(['$routeProvider', function($routeProvider) {
	// default view is home
	
	$routeProvider.otherwise({
		redirectTo: '/home'
	});
}]);

